# HelloCNN

[![pipeline status](https://gitlab.com/zchrissirhcz/cnn/badges/master/pipeline.svg)](https://gitlab.com/zchrissirhcz/cnn/-/commits/master) [![coverage report](https://gitlab.com/zchrissirhcz/cnn/badges/master/coverage.svg)](https://gitlab.com/zchrissirhcz/cnn/-/commits/master)

Implement CNN in a Bottom-Up manner.

## HelloCNN1
前置： Python 基础， Git 基础， VSCode 基础。

目标： 最简单的前向计算

细节： 单个标量输入， 单个神经元， 前向计算

## HelloCNN2
前置： HelloCNN1

目标： 最简单的能训练的神经网络； 能解决最简单的回归问题。

细节： 单个样本（标量）， 只有一个神经元。
针对回归问题， 引入了 (mean) square error 作为 loss 公式。
使用 BP 和 Gradient Descent 来更新参数。 能处理直线拟合问题。

能够训练多个 epoch （但还没有 batch 的概念）。

## HelloCNN3
前置： HelloCNN1 和 HelloCNN2。

目标： 能训练的、用于二分类问题的最简单神经网络。

单个样本（标量）， 只有一个神经元。
针对二分类问题， 引入交叉熵损失函数作为 loss 公式。
BP 和 Gradient Descent。
处理的二分类问题： 判断点投篮距离和是否投中（0/1）的关系。