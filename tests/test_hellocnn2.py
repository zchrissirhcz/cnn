from mytest import MyTestRunner
import torch
import unittest
import numpy as np

import sys, os

DEBUG = False

if DEBUG:
    from loguru import logger

sys.path.insert(0,
    os.path.join(
        os.path.dirname(__file__), # 当前文件所在目录（不是绝对路径）
        '..' # 上级目录
    )
)
from HelloCNN2.neuron import get_train_data
from HelloCNN2.neuron import train_hello

torch.set_default_tensor_type(torch.DoubleTensor)

class Net(torch.nn.Module):
    def __init__(self, n_feature, n_hidden):
        super(Net, self).__init__()
        self.fc = torch.nn.Linear(n_feature, n_hidden, bias=True)   # output layer
        # 手动给参数初始化。为了和naive实现比对。
        self.fc.weight = torch.nn.Parameter(torch.tensor([[-3.0]]))
        self.fc.bias = torch.nn.Parameter(torch.tensor([0.1]))

    def forward(self, x):
        return self.fc(x)

def train_torch(X, Y, lr=0.1):
    """
    :param X: 1维数组
    :param Y: 1维数组
    X, Y = get_train_data()
    train_hello(X, Y)

    :return: np.ndarray
    """
    if DEBUG:
        fout = open("cnn2_torch.log", "w")
        logger.add(fout, format="{message}")

    net = Net(n_feature=1, n_hidden=1)
    #net = net.double()
    optimizer = torch.optim.SGD(net.parameters(), lr=lr, momentum=0)
    loss_func = torch.nn.MSELoss()  # this is for regression mean squared loss

    # learning stage
    max_epoch = 5
    for epoch in range(max_epoch):
        loss_total = 0

        X, Y = get_train_data()
        for i in range(len(X)):
            x = X[i]
            x_in = torch.unsqueeze(torch.tensor(np.array([x]), requires_grad=True), dim=1)
            out = net(x_in)
            out_numpy = out.detach().numpy()
            
            y_pred = out_numpy

            gt = torch.unsqueeze(torch.tensor(np.array([Y[i]]), requires_grad=False), dim=1)
            loss = loss_func(out, gt)
            loss_total += float(loss)

            if DEBUG:
                logger.debug("i={:d}, y_pred={:f}, loss={:f}".format(i, float(y_pred), float(loss)))

            #print("x_in:", x_in.data.detach().numpy(), "gt:", gt.data.detach().numpy(), "out:", out_numpy, "weight:", net.fc.weight.data.detach().numpy(), "bias:", net.fc.bias.data.detach().numpy(), "loss:", loss.double().data.detach().numpy())

            optimizer.zero_grad()   # clear gradients for next train
            loss.backward()         # backpropagation, compute gradients
            optimizer.step()        # apply gradients

        #print(">>> epoch {:d}, total_loss={:f}".format(epoch, loss_total))

    # inference stage
    Y_pred = np.ndarray((Y.shape[0], 1))
    for i in range(len(X)):
        x = X[i]
        x_in = torch.unsqueeze(torch.tensor(np.array([x]), requires_grad=True), dim=1)
        out = net(x_in)
        out_numpy = out.detach().numpy()
        
        y_pred = out_numpy
        Y_pred[i] = y_pred

    if DEBUG:
        fout.close()

    return Y_pred

class HelloCNN2_Test(unittest.TestCase):
    def test_hellocnn(self):
        X, Y = get_train_data()
        Y_pred_torch = train_torch(X, Y)
        Y_pred_hello = train_hello(X, Y)
        self.assertEqual(Y_pred_torch.shape, Y_pred_hello.shape)
        for i in range(Y_pred_torch.shape[0]):
            self.assertAlmostEqual(float(Y_pred_torch[i]), float(Y_pred_hello[i]), places=3)

if __name__ == '__main__':
    unittest.main(testRunner=MyTestRunner())
