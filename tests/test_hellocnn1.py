from mytest import MyTestRunner
import torch
import unittest
import numpy as np

import sys, os

sys.path.insert(0,
    os.path.join(
        os.path.dirname(__file__), # 当前文件所在目录（不是绝对路径）
        '..' # 上级目录
    )
)
from HelloCNN1.neuron import neuron_compute_hello

class Net(torch.nn.Module):
    def __init__(self, n_feature, n_hidden):
        super(Net, self).__init__()
        self.fc = torch.nn.Linear(n_feature, n_hidden, bias=True)   # output layer
        # 手动给参数初始化。为了和naive实现比对。
        self.fc.weight = torch.nn.Parameter(torch.tensor([[2.5]]))
        self.fc.bias = torch.nn.Parameter(torch.tensor([0.1]))

    def forward(self, x):
        return torch.sigmoid(self.fc(x))


def neuron_compute_torch(X):
    n_feature = 1
    n_hidden = 1
    net = Net(n_feature=1, n_hidden=1)     # define the network
    net = net.double() # https://stackoverflow.com/questions/60495029/runtimeerror-expected-object-of-scalar-type-double-but-got-scalar-type-float-fo

    # for debug purpose, you may use this:
    # for name, param in net.named_parameters():
    #     print(name, param)
    #print(net)  # net architecture

    x = torch.unsqueeze(torch.tensor(X), dim=1)  # x data (tensor), shape=(100, 1)
    Y = net(x)
    return Y.detach().numpy()

class HelloCNN1_Test(unittest.TestCase):
    def test_hellocnn(self):
        X = np.arange(-1, 1, 0.2)
        Y_hello = neuron_compute_hello(X)
        Y_torch = neuron_compute_torch(X)
        #self.assertTrue(np.allclose(Z_hello, Z_torch))
        #self.assertTrue(np.array_equal(Z_hello, Z_torch, equal_nan=True))
        #self.assertSequenceEqual(Z_hello.tolist(), Z_torch.tolist())
        for i in range(len(Y_hello)):
            self.assertAlmostEqual(float(Y_hello[i]), float(Y_torch[i]), places=7)

if __name__ == '__main__':
    unittest.main(testRunner=MyTestRunner)
    #X = np.arange(-1, 1, 0.2)
    #neuron_compute_torch(X)