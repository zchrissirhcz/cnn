#!/bin/bash

for item in `ls test*.py`; do
    echo ">>> Start running test for $item"
    python $item
    echo "<<< Done of current test"
done


# coverage run --source=HelloCNN1 test_hellocnn1.py
# coverage run --source=HelloCNN2 test_hellocnn2.py
# coverage combine
# coverage report -m --skip-empty
