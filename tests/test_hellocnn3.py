from mytest import MyTestRunner
import torch
import unittest
import numpy as np

import sys, os

DEBUG = False

if DEBUG:
    from loguru import logger

sys.path.insert(0,
    os.path.join(
        os.path.dirname(__file__), # 当前文件所在目录（不是绝对路径）
        '..' # 上级目录
    )
)
from HelloCNN3.neuron import get_train_data
from HelloCNN3.neuron import train_hello

torch.set_default_tensor_type(torch.DoubleTensor)

class Net(torch.nn.Module):
    def __init__(self, n_feature, n_hidden):
        super(Net, self).__init__()
        self.fc = torch.nn.Linear(n_feature, n_hidden, bias=True)
        # 手动给参数初始化。为了和naive实现比对。
        self.fc.weight = torch.nn.Parameter(torch.tensor([[-1.0]]))
        self.fc.bias = torch.nn.Parameter(torch.tensor([1.0]))
        self.sigmoid = torch.nn.Sigmoid()

    def forward(self, x):
        return self.sigmoid(self.fc(x))
    
def train_torch(X, Y, lr=0.001):
    if DEBUG:
        fout = open("cnn3_torch.log", "w")
        logger.add(fout, format="{message}")

    net = Net(n_feature=1, n_hidden=1)
    optimizer = torch.optim.SGD(net.parameters(), lr=lr, momentum=0)
    #loss_func = torch.nn.CrossEntropyLoss()
    loss_func = torch.nn.BCELoss() # 为什么用 CrossEntropyLoss 不行？

    max_epoch = 5
    for epoch in range(max_epoch):
        loss_total = 0
        
        X, Y = get_train_data()
        for i in range(len(X)):
            x = X[i]
            x_in = torch.unsqueeze(torch.tensor(np.array([x]), requires_grad=True), dim=1)
            out = net(x_in)
            out_numpy = out.detach().numpy()

            y_pred = out_numpy

            gt = torch.unsqueeze(torch.tensor(np.array([Y[i]]), requires_grad=False), dim=1)
            
            loss = loss_func(out, gt)
            loss_total += float(loss)

            if DEBUG:
                logger.debug("i={:d}, y_pred={:f}, loss={:f}".format(i, float(y_pred), float(loss)))

            optimizer.zero_grad()   # clear gradients for next train
            loss.backward()         # backpropagation, compute gradients
            optimizer.step()        # apply gradients

    # inference stage
    Y_pred = np.ndarray((Y.shape[0], 1))
    for i in range(len(X)):
        x = X[i]
        x_in = torch.unsqueeze(torch.tensor(np.array([x]), requires_grad=True), dim=1)
        out = net(x_in)
        out_numpy = out.detach().numpy()
        
        y_pred = out_numpy
        Y_pred[i] = y_pred

    if DEBUG:
        fout.close()

    return Y_pred


class HelloCNN3_Test(unittest.TestCase):
    def test_hellocnn(self):
        X, Y = get_train_data()
        Y_pred_torch = train_torch(X, Y)
        Y_pred_hello = train_hello(X, Y)
        self.assertEqual(Y_pred_torch.shape, Y_pred_hello.shape)
        for i in range(Y_pred_torch.shape[0]):
            self.assertAlmostEqual(float(Y_pred_torch[i]), float(Y_pred_hello[i]), places=3)

if __name__ == '__main__':
    unittest.main(testRunner=MyTestRunner())
