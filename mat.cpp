#include <iostream>
#include <stdlib.h>
#include "mat.h"

using namespace std;

int randint(int a, int b) {
    int len = b - a + 1;
    int t = rand() % len;
    return t + a;
}

//补边
//目前仅仅考虑2D情况
void copy_make_border(const Mat& mat, Mat& padded_mat, const Mat& kernel, Pad pad) {
    int w = mat.w;
    int pl = pad.left;
    int pr = pad.right;
    int s = kernel.stride;
    int k = kernel.width;
    int ew = (w+pl+pr-k) % s; // extra tail in width
    int ow = (w+pl+pr-ew-k)/s + 1;
    
    int h = mat.h;
    int pt = pad.top;
    int pb = pad.bottom;
    int eh = (h+pt+pb-k) % s; // extra tail in height
    int oh = (h+pt+pb-eh-k)/s + 1;

    padded_mat.create(ow, oh, mat.c);
    
    switch(pad.pad_type) {
        case ZERO_PAD:
        {
            //for(int i=0; i<padded_mat.c; i++) {
                // first pt rows
                for (int j=0; j<pad.top; j++) {
                    for(int k=0; k<padded_mat.w; k++){
                        padded_mat.set(j, k, 0);
                    }
                }

                for (int j=pt; j<pt+mat.h; j++) {
                    for(int k=0; k<pad.left; k++) {
                        padded_mat.set(j, k, 0);
                    }
                    for(int k=pad.left; k<pad.left+mat.w; k++){
                        padded_mat.set(j, k, mat.at(j-pt, k-pad.left));
                    }
                    for(int k=pad.left+mat.w; k<padded_mat.w; k++){
                        padded_mat.set(j, k, 0);
                    }
                }

                for (int j=pt+mat.h; j<padded_mat.h; j++){
                    for(int k=0; k<padded_mat.w; k++){
                        padded_mat.set(j, k, 0);
                    }
                }


            //}
        }
        break;
        default:
        {
            fprintf(stderr, "Not supported padding type, line %d, file %s\n", __LINE__, __FILE__);
        }
    }

}

//最naive的2d卷积：单通道，不padding，不做计算优化，直接sliding window式的计算
Mat convolve2d(Mat input, Mat kernel){
    int outw = input.w - kernel.w + 1;
    int outh = input.h - kernel.h + 1;
    Mat output(outw, outh);
    for(int h=0; h<output.h; h++){
        for(int w=0; w<output.w; w++){
            int v = 0;
            for(int hh=0; hh<kernel.h; hh++){
                for(int ww=0; ww<kernel.w; ww++){
                    v += input.at(h+hh, w+ww)*kernel.at(hh, ww);
                }
            }
            output.set(h, w, v);
        }
    }
    return output;
}

int main() {
    // 第一次计算，简单点，input(C=1,H=3,W=3), kernel(C=1,H=2,W=2)
    Mat input(3, 3);

    int t; // random number
    for (int i = 0; i < input.h; i++) {
        for(int j=0; j<input.w; j++) {
            t = randint(0, 10);
            input.set(i, j, t);
        }
    }
    input.show();   

    Mat kernel(2, 2);
    for (int i = 0; i < kernel.h; i++) {
        for (int j=0; j<kernel.w; j++){
            kernel.set(i, j, randint(0, 5));
        }
    }
    kernel.show();

    Mat padded_input;
    Pad pad;
    pad.left = 1;
    pad.right = 2;
    pad.top = 1;
    pad.bottom = 1;
    pad.pad_type = ZERO_PAD;
    pad.pad_value = 0;
    copy_make_border(input, padded_input, kernel, pad);
    padded_input.show();

    Mat output = convolve2d(input, kernel);
    output.show();

    Mat output_of_padded_input = convolve2d(padded_input, kernel);
    output_of_padded_input.show();

    return 0;
}

