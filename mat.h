#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

class Mat {
public:
    Mat(int w, int h);
    void create(int w, int h);
    void show();
    int* data;
    int w;
    int h;
    void set(int i, int j, float v);
    int& at(int i, int j);
};

inline Mat::Mat(int _w, int _h)
    : data(0){
    create(_w, _h);
}

inline void Mat::create(int _w, int _h){
    w = _w;
    h = _h;
    data = (int*)malloc(sizeof(int)*w*h);
}

inline void Mat::show(){
    printf("-----\n");
    for(int i=0; i<h; i++){
        for(int j=0; j<w; j++){
            printf("%d, ", data[i*w+j]);
        }
        printf("\n");
    }
}

inline void Mat::set(int i, int j, float v){
    data[i*w + j] = v;
}

inline int& Mat::at(int i, int j){
    return data[i*w + j];
}

typedef enum PadType{
    ZERO_PAD,
    SAME_PAD
};

struct Pad{
    int left;
    int right;
    int top;
    int bottom;
    PadType pad_type;  
    float pad_value;
};

struct Kernel{
    
}
