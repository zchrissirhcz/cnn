# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt

DEBUG = False

if DEBUG:
    from loguru import logger

class Neuron:
    def __init__(self, w, b, lr=0.2) -> None:
        self.w = w
        self.b = b
        self.lr = lr
    
    def forward(self, x):
        # u = x * w + b   注意这次的激活函数是 sigmoid(u) = 1/(1+exp(-u))
        self.x = x
        u = x * self.w + self.b
        self.y = 1.0 / (1.0 + np.exp(-u))
        return self.y
    
    def backward(self, grad_y):
        """
        grad_y 是 dL/dy 的结果
        对于 HelloCNN2 来说， 只有一层网络且这层网络只有一个神经元， 因此 y 值是确定的
        因而， grad_y 也是确定的， 能根据损失函数的形式，写出具体表达式 y-t
        （不过这里是传入的， 这里不知道具体形式也没关系）

        对于后续多层网络来说， 每一层只关心自己层的输出 y 对应的 grad_y 即可
        """
        dy_du = self.y * (1 - self.y)
        self.grad_w = grad_y * dy_du * self.x
        self.grad_b = grad_y * dy_du

        self.w -= self.lr * self.grad_w
        self.b -= self.lr * self.grad_b

def get_train_data():
    X = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], dtype=np.float64)
    Y = np.array([1, 1, 1, 1, 1, 1, 0, 1, 0, 1,  1,  0,  0,  1,  1,  0,  0,  0,  1,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0], dtype=np.float64)
    return X, Y

def train_hello(X, Y, lr=0.001, visualize=False):
    if DEBUG:
        fout = open("cnn3_hello.log", "w")
        logger.add(fout, format="{message}")

    X, Y = get_train_data()
    #neuron = Neuron(-0.2, 2.5, 0.15)
    #neuron = Neuron(-1, 1.0, 0.1)
    neuron = Neuron(-1, 1.0, lr)
    max_epoch = 5

    if visualize:
        plt.ion()

    # learning stage
    for epoch in range(max_epoch):
        loss_total = 0
        for i in range(len(X)):
            x = X[i]
            t = Y[i]
            y_pred = neuron.forward(x)
            if (t==1):
                grad_y = -1.0 / y_pred
                loss = - np.log(y_pred)
            else: # t==0
                grad_y = 1.0 / (1 - y_pred)
                loss = - np.log(1 - y_pred)
            loss_total += loss

            if DEBUG:
                logger.debug("i={:d}, y_pred={:f}, loss={:f}".format(i, float(y_pred), float(loss)))

            neuron.backward(grad_y)
        
        if visualize and epoch % 1 == 0:
            Y_pred = np.ndarray(Y.shape)
            for i in range(len(Y_pred)):
                Y_pred[i] = neuron.forward(X[i])

            plt.cla()
            plt.scatter(X, Y, color='blue', label='Actual Outcome')
            plt.scatter(X, Y_pred, color='green', label='Predicted Log Odds')
            plt.legend()
            plt.text(0, 0, 'Loss=%.4f' % (loss_total), fontdict={'size': 15, 'color':  'red'})
            print(loss_total)
            plt.pause(1)
        
        print(">>> epoch {:d}, total_loss={:f}".format(epoch, loss_total))

    # inference stage
    Y_pred = np.ndarray((Y.shape[0], 1))
    for i in range(len(X)):
        x = X[i]
        y_pred = neuron.forward(x)
        Y_pred[i] = y_pred

    if visualize:
        plt.ioff()
        plt.show()
    
    return Y_pred

#train()
def plot_test():
    X, Y = get_train_data()
    fig, ax = plt.subplots()
    ax.scatter(X, Y)
    ax.set_xlabel('Distance from Basket')
    ax.set_ylabel('Outcome from shot')
    ax.legend()
    plt.show()

#plot_test()
if __name__ == '__main__':
    X, Y = get_train_data()
    lr = 0.001
    train_hello(X, Y, lr, visualize=True)