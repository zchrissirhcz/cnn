"""
@file test.py 
@description 作为 main.ipynb 的单元测试。 和 pytorch 比对结果。
"""

import numpy as np
import matplotlib.pyplot as plt

class Neuron:
    def __init__(self) -> None:
        self.w = 2.5
        self.bias = 0.1
    
    def forward(self, x):
        # sigmoid(x * w + b)
        self.u = x * self.w + self.bias
        self.z = 1/(1+np.exp(-self.u)) # sigmoid
        return self.z

def neuron_compute_hello(X):
    """
    一系列的输入数据，送给神经元进行计算
    绘制所有计算结果到一张图上
    """
    #X=np.arange(-1, 1, 0.2)

    U = np.zeros(X.shape)
    Z = np.zeros(X.shape)
    neuron = Neuron()
    for i in range(len(X)):
        Z[i] = neuron.forward(X[i])
        U[i] = neuron.u
    return Z

    fig, ax = plt.subplots()
    ax.plot(X, U, label='u=w*x+b')
    ax.plot(X, Z, label='z=sigmoid(u)=sigmoid(w*x+b)')
    ax.set_xlabel('x')
    ax.legend()
    plt.show()
