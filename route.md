## why this
一方面是因为很早就想自己写一个实现，但是一直没有能真正去做。
另一方面是因为现在的日常工作就是维护框架，对于基本的cnn实现都不清楚的话，寸步难行。

预计有3个branch：
1. 最最最naive的实现。用来作为标准结果。
2. 常规的优化方式的手动实现。例如im2col
3. 基于neon优化的实现方式
4.* learning的优化（反向传播时）

第一版: 最最最naive的实现
===
## Mat类
从ncnn的Mat抄袭了一点点。最快得实现方式，最少的功能。好吧，内存也许有泄漏，暂时不管那么多。

## 平凡卷积
### 2D卷积
只考虑int型。input和kernel的取值来自[0,10]之间的随机整数，并且尺寸也超级小。用来直观的查看结果。
这一版本绝对不能出错。因为它用来产生标准结果呀。

### 常见卷积padding方式和实现
tensorflow中又两种：same; valid
- same

- valid

来看看这个帖子：https://adeshpande3.github.io/A-Beginner%27s-Guide-To-Understanding-Convolutional-Neural-Networks-Part-2/


### 3D卷积


- relu
- softmax

- group卷积
- depthwise卷积
