#include <stdio.h>
#include <string>
#include <vector>

template<typename Tp>
class Neuron
{
public:
    Neuron(Tp _w, Tp _b, Tp _lr=0.2):
        w(_w), b(_b), lr(_lr)
    {}

    Tp forward(Tp _x)
    {
        x = _x;
        Tp u = x * w + b;
        return u;
    }

    void backward(Tp grad_y)
    {
        Tp grad_w = grad_y * x;
        Tp grad_b = grad_y;

        w -= lr * grad_w;
        b -= lr * grad_b;
    }

public:
    Tp w;
    Tp b;
    Tp lr;

    Tp x;
};

int main()
{
    //using data_type = double;
    using data_type = float;

    //std::vector<data_type> X = {-1.0, 0.0, 1.0};
    //std::vector<data_type> Y = {0.9646440511781974, 3.014556809911726, 4.980829012821493};
    
    std::vector<data_type> X = {
        3.3, 4.4, 5.5, 6.71, 6.93, 4.168, 
        9.779, 6.182, 7.59, 2.167, 7.042, 
        10.791, 5.313, 7.997, 3.1
    };

    std::vector<data_type> Y = {
        1.7, 2.76, 2.09, 3.19, 1.694, 1.573, 
        3.366, 2.596, 2.53, 1.221, 2.827, 
        3.465, 1.65, 2.904, 1.3
    };

    //Neuron<data_type> neuron(-3.0, 0.1, 0.1);
    Neuron<data_type> neuron(-3.0, 0.1, 0.001);

    for (int i=0; i<X.size(); i++)
    {
        data_type x = X[i];
        data_type t = Y[i];
        data_type y_pred = neuron.forward(x);
        data_type loss = (y_pred - t) * (y_pred - t);
        data_type grad_y = 2 * (y_pred - t);
        neuron.backward(grad_y);

        printf("i=%d, x: %lf, gt: %lf, y_pred: %lf, weight: %lf, bias: %lf, loss: %lf\n",
            i, x, t, y_pred, neuron.w, neuron.b, loss
        );
    }

    return 0;
}