2021年09月08日23:11:17

在比较自己的实现、基于pytorch的实现的结果时， 一些要点记录如下。

## 数据类型

numpy 默认用 float64， 也就是 double 类型。 可以在创建 ndarray 时手动指定 `dtype=float32`， 但官方没提供设定全局默认为float的方法。

torch.tensor 则默认用 float32。 numpy.ndarray 转 torch.tensor 在不指定类型情况下得到 double 类型的 tensor， 网络（模型）会报错说不支持。一种方法是：

```python
net = get_model()
net = net.double() #!!
```

为了比较结果方便， 也可全局设定torch.tensor默认用double：

```python
torch.set_default_tensor_type(torch.DoubleTensor)
```
https://stackoverflow.com/questions/52199728/how-to-use-double-as-the-default-type-for-floating-numbers-in-pytorch

为了避免原生 python 的 double 类型与 numpy 的 floa64 的不一致， 自己的实现里的网络参数，从原生类型改为numpy的ndarray。

## 损失函数

收先前一些教程的影响， 我在 MSELoss 的计算上用了 `0.5 * (y_pred - t) * (y_pred - t)`， 0.5 为了求导方便。 而 PyTorch 的 MSELoss 文档中写的公式并没有带 0.5, 从结果比对来看， PyTorch 也确实没用 0.5。

## 随机数

先前在制作 gt 的时候用随机数增加了多样性， 但这也导致了， hello 以及 torch 实现的模型， 拿到的输入不一样， 进而第一次结果就存在差异， 后续差异的排查更加困难。

添加了种子，确保了结果稳定性：
```python
np.random.seed(0)
```

## 还是不一样
用 C++ 把 numpy 版本重新实现了一下。 结果和 numpy 版本基本一致。 感觉是 pytorch 有 bug， 或者我哪里用的不对？？

grad_y = (y_pred - t)  =>  grad_y = 2 * (y_pred - t)

## 像torch一样的optimizer
```python
    optimizer = torch.optim.SGD(net.parameters(), lr=0.1, momentum=0)
```

则 optimizer 可以接管各种参数的更新。换言之定义 optimizer 的时候就是把各种参数在 optimizer 中注册。

简单尝试了一下， python 的函数传参， 如果是原生类型则无法改变值， 相当于拷贝； 如果是类类型，则可以改变类实例中的成员变量的值， 也就是说相当于引用。

<del>因此需要自行定义tensor类。</del> 完全用 numpy.ndarray 即可: self.w = np.array(w)