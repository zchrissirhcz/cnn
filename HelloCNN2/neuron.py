import numpy as np
import matplotlib.pyplot as plt

DEBUG = False

if DEBUG:
    from loguru import logger


class Tensor:
    def __init__(self, data) -> None:
        self.data = data

class Neuron:
    def __init__(self, w, b, lr=0.2) -> None:
        self.w = np.array(w)
        self.b = np.array(b)
        self.lr = np.array(lr)
    
    def forward(self, x):
        # u = x * w + b   注意这次没有激活函数，或者说是identity激活函数
        self.x = x
        u = x * self.w + self.b
        return u
    
    def backward(self, grad_y):
        """
        grad_y 是 dL/dy 的结果
        对于 HelloCNN2 来说， 只有一层网络且这层网络只有一个神经元， 因此 y 值是确定的
        因而， grad_y 也是确定的， 能根据损失函数的形式，写出具体表达式 y-t
        （不过这里是传入的， 这里不知道具体形式也没关系）

        对于后续多层网络来说， 每一层只关心自己层的输出 y 对应的 grad_y 即可
        """
        # grad_w 是 dL/dw 的简写， 意思是损失L对w的梯度， 也就是梯度 \nabla 在 w 方向的分量
        # 根据链式法则， dL/dw = dL/dy * dy/dw = grad_y * x
        # dL/db = dL/dy * dy/db = grad_y
        self.grad_w = grad_y * self.x
        self.grad_b = grad_y

        self.w -= self.lr * self.grad_w
        self.b -= self.lr * self.grad_b

def get_train_data():
    # 准备输入和 ground truth (target)
    np.random.seed(0)
    
    X = np.linspace(-1.0, 1.0, 100) #start, end, number
    Y = np.ndarray(X.shape)
    for i in range(len(X)):
        Y[i] = 2.0 * X[i] + 3.0 + np.random.uniform(-0.2, 0.1)
    return X, Y

def train_hello(X, Y, lr=0.1):
    """
    X, Y = get_train_data()
    train_hello(X, Y)
    """
    if DEBUG:
        fout = open("cnn2_hello.log", "w")
        logger.add(fout, format="{message}")

    neuron = Neuron(np.array([-3.0]), np.array([0.1]), lr=np.array(lr))
    
    max_epoch = 5
    #plt.ion()

    # learning stage
    for epoch in range(max_epoch):
        loss_total = 0
    
        for i in range(len(X)):
            x = X[i]
            t = Y[i]
            
            y_pred = neuron.forward(x)
            
            loss = ((y_pred - t)*(y_pred - t))
            loss_total += float(loss)

            if DEBUG:
                logger.debug("i={:d}, y_pred={:f}, loss={:f}".format(i, float(y_pred), float(loss)))
            
            #loss_total += loss
            grad_y = 2 * (y_pred - t) # dL/dy
            neuron.backward(grad_y)

            #print("x:", x, "gt:", t, "y_pred:", y_pred, "weight:", neuron.w, "bias:", neuron.b, "loss:", loss)
            
            # print("in forward() stage, Y_pred:")
            # print(Y_pred.shape)
            # print(Y_pred)
            
            #if epoch % 1 == 0:
            # Y_pred = np.ndarray((Y.shape[0], 1))
            # for i in range(len(Y_pred)):
            #     Y_pred[i] = neuron.forward(X[i])

            # print("after backward() stage, Y_pred:")
            # print(Y_pred.shape)
            # print(Y_pred)
            # plt.cla()
            # plt.scatter(X, Y)
            # plt.plot(X, Y_pred, color='red')
            # plt.text(0, 0, 'Loss=%.4f' % (loss_total), fontdict={'size': 15, 'color':  'red'})
            # print(loss_total)
            # plt.pause(1)
        
        #print(">>> epoch {:d}, total_loss={:f}".format(epoch, loss_total))

    # inference stage
    Y_pred = np.ndarray((Y.shape[0], 1))
    for i in range(len(X)):
        x = X[i]
        y_pred = neuron.forward(x)
        Y_pred[i] = y_pred

    # plt.ioff()
    # plt.show()
    if DEBUG:
        fout.close()

    return Y_pred


